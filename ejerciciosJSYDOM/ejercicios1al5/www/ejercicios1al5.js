"use strict";

//EJERCICIO 1

async function usuarios(numero) {
  const url = `https://randomuser.me/api?results=${numero}`;
  const getData = await fetch(url);
  const getDataJson = await getData.json();

  const datosUsuarios = getDataJson.results;

  let usuarios = [];
  for (const busquedaUsuarios of datosUsuarios) {
    usuarios.push(
      `${busquedaUsuarios.login.username}, ${busquedaUsuarios.name.first}, ${busquedaUsuarios.name.last}, ${busquedaUsuarios.gender}, ${busquedaUsuarios.location.country}, ${busquedaUsuarios.email}, ${busquedaUsuarios.picture.medium}`
    );
  }
  return usuarios;
}
console.log(usuarios(7));

//transformar una promesa pendiente en una respuesta
usuarios(7).then((valor) => console.log(valor));

/* -------------------------------------------------------------------
ejercicio2 */
let segundo = 0;
let minuto = 0;
let hora = 0;
let dia = 0;
let tiempo = setInterval(() => {
  if (segundo > 59) {
    minuto++;
    segundo = 0;
  } else if (minuto > 59) {
    hora++;
    minuto = 0;
  } else if (hora > 23) {
    dia++;
    hora = 0;
  }
  console.log(
    `Tu tiempo es de ${dia} dias, ${hora} horas, ${minuto} minutos y ${segundo} segundos a tope!`
  );
  segundo += 5;
}, 5000);

//EJERCICIO 3

function convertir(numero, base) {
  if (base === 2) {
    return +numero.toString(base);
  } else if (base === 10) {
    return numero
      .toString()
      .split("")
      .reverse()
      .reduce((acumulator, value, index) => {
        return value === "1" ? acumulator + 2 ** index : acumulator;
      }, 0);
  }
}

//console.log(convertir(1000, 10));
console.log(convertir(8, 2));

// EJERCICIO 4

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

function nuevoArray(parametro) {
  return parametro.filter(
    (valor, index, array) => array.indexOf(valor) === index
  );
}

console.log(nuevoArray(names));

//EJERCICIO 5

const url = "https://rickandmortyapi.com/api/";

async function rickAndMorti(url) {
  const datos = await fetch(url);
  const respuesta = await datos.json();

  const datosEpisodios = await fetch(respuesta.episodes);

  const respuestaEpisodios = await datosEpisodios.json();
  const resultado = respuestaEpisodios.results;

  const dia = resultado.filter(
    (day) =>
      day.air_date >= "January 1,2014" && day.air_date <= "January 31,2014"
  );
  const urlPersonajes = dia.map((personajes) => personajes.characters);
  const arrayPersonajes = [];
  for (const urlCharacters of urlPersonajes) {
    arrayPersonajes.push(...urlCharacters);
  }
  const datosDefinitivos = [];
  for (const nombre of arrayPersonajes) {
    const nombrePedido = await fetch(nombre);
    const respuestaPedido = await nombrePedido.json();
    datosDefinitivos.push(`${respuestaPedido.name}`);
  }
  console.log(datosDefinitivos);
}

rickAndMorti(url);
