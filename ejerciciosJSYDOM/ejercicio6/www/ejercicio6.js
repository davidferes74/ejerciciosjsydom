"use strict";

//EJERCICIO 6

const parrafos = document.querySelectorAll("p");
console.log(parrafos);

for (const palabras of parrafos) {
  const juntaPalabras = palabras.innerHTML; //innerHTML saca y mete el texto dentro del párrafo
  const palabrasFiltradas = juntaPalabras
    .split(" ") //split transforma de un string a un array
    .filter((size) => size.length > 5);
  //console.log(palabrasFiltradas);
  palabras.innerHTML = palabras.textContent
    .split(" ")
    .map((palabra) => {
      if (palabrasFiltradas.indexOf(palabra) > -1) {
        return `<span> ${palabra}</span>`;
      } else {
        return palabra;
      }
    })
    .join(" "); // join une el array en un string...al revés que el split
}

const span = document.querySelectorAll("span");
for (const subra of span) {
  subra.style.textDecoration = "underline";
}
