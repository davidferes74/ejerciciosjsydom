"use strict";

//EJERCICIO 8

//---------------------------------------------------------------------------------

const body = document.querySelector("body");
const h1 = document.createElement("h1");
const h2 = document.createElement("h2");

const botones = document.createElement("div");
const start = document.createElement("button");
const finish = document.createElement("button");
const reset = document.createElement("button");

body.appendChild(h1);
body.appendChild(h2);
body.appendChild(botones);
botones.appendChild(start);
botones.appendChild(finish);
botones.appendChild(reset);
h1.textContent = "Cronómetro oficial de la UEFA";
start.textContent = "Start";
finish.textContent = "Finish";
reset.textContent = "Reset";

let segundos = 0;
let minutos = 0;

function cronometro() {
  if (segundos > 59) {
    minutos++;
    segundos = 0;
  }
  h2.textContent = `Minutos: ${minutos} Segundos: ${segundos}`;
  segundos++;
}
cronometro();
h1.style.color = "green";
h2.style.fontSize = "35px";
const botoness = body.querySelectorAll("button");
for (let botoncitos of botoness) {
  botoncitos.style.padding = "40px";
  botoncitos.style.backgroundColor = "#ffbd4a";
  botoncitos.style.marginLeft = "5px";
  botoncitos.style.fontSize = "25px";
}

let startCronometro;

start.addEventListener("click", empezar);
finish.addEventListener("click", terminar);
reset.addEventListener("click", resetear);

function empezar() {
  startCronometro = setInterval(cronometro, 1000);
}

function terminar() {
  clearInterval(startCronometro);
}

function resetear() {
  minutos = 0;
  segundos = 0;
  h2.textContent = `Minutos: ${minutos} Segundos: ${segundos}`;
}
